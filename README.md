# carbon_indicator_iota2

- Annual Carbon Flux Indicator.
- Develop for the SOCCROP project.
- L. Arnaud. CESBIO - INREA 2021
- contact: ludovic.arnaud@inrae.fr

## Basic Workflow (can be improved):

1. Downloading ERA5 data according to a specific S2 tile list and agricultural year (AY). For instance, to do so for the T30TYP and T31TCJ tiles for the AY 2019, run the commands:
    ```sh
    cd path/to/write/the/data
    python3 ./path/to/scripts/download_and_encodeERA5_v1.0.py -r path/to/T30TYP_ref.tif path/to/T31TCJ_ref.tif -t T30TYP T31TCJ -y 2019 -e North
    ```
    where:
    - T30TYP_ref.tif and T31TCJ_ref.tif are S2 reference raster images
    - North indicates the tile are in the North hemisphere
    
    To run this script, python3 needs to have the following libraries installed:
    - xarray
    - cdsapi
    - rasterio
    
    Depending on the ERA5 API site charge, the download takes about 3 min per tile. The output data can be found at **path/to/write/the/data/outputs** in NETCDF files that contain an encoded form of the daily solar radiation and the 2m temperature for the corresponding AY. The files are named according to a patern of the form **T31TCJ_2019_ERA5_encoded_data.nc**. Each file weight less than 1MB.
    
    This command can be run on a lot of tiles thanks to a job array. However, parallele connection to the ERA5 API is not recommended, so the number of tiles should be adjusted.
    
    2. Convert the ERA5 NETCDF file to the sentinel-2 10m grid. Per tile, it can be done with the OTB command line:
    ```sh
    otbcli_Superimpose -inr  path/to/T31TCJ_ref.tif -inm  path/to/T31TCJ_2019_ERA5_encoded_data.nc -interpolator nn -out path/to/T31TCJ_2019_ERA5_encoded_data.tif uint32
    ```
    This command can also be run on a lot of tiles thanks to a job array. In this case, disk usage should be take in account because the output raster per tile weight about 185GB

3. Produce Carbon Indicator Tier 1 . It can be done thanks to a iota2 external feature. iota2 configuration template can be found in the config directory of this project.

    ---
    **Important Note**

    At the moment, the line corresponding to the use of ERA5 data should remains commented. Those exogeneous data are meant to point to the ERA5 encoded data previoulsy produce with a template file name that i.e. something on the form: path/to/era5data/$TILE_2019_encoded_data.tif. However, those data seams to be too volumnious to be handled without iota2 memory error.

    ---








