######################################################
#                  SOCCROP project                   #
#  Calculate Carbon Fluxes Indicator at pixels level #
#    External feature for iota2 processing chain     # 
#                                                    # 
#             L. Arnaud - Cesbio - INRAE             #
#                  Creation: 15/11/2021              #
#               Last update: 19/02/2023              #
######################################################


def robust_ndvi(nir_masked,red_masked):
    """
    From Pratvi Kalavadiya 
    """
    import numpy.ma as ma

    nir = ma.masked_where(nir_masked <= 0,nir_masked)
    red = ma.masked_where(red_masked <= 0,red_masked)

    ndvi = ((nir-red)/(nir+red))

    return ndvi

def calculate_correlation_functions(N,Rg,Ta):
    """
    Calculate correlation functions between NDVI, Rg and Ta 
    Parameters
    ----------
    NDVI : float numpy.array(X_len,Y_len,T_len)
         Normalized Difference Vegetation Index
    Rg   : float numpy.array(X_len,Y_len,T_len)
         Solar radiation in W/m2
    Ta   : float numpy.array(X_len,Y_len,T_len)
         Atmospheric temperature in °C 
                   
    Returns
    -------
    CorrFunc : list of float numpy.array(X_len,Y_len,)
    """
    import numpy as np

    # Powers of Order < 3 
    #powers = [[1,0,1],[0,1,1],[1,0,2],[0,1,2],[2,1,0],[1,1,2]]
    powers = [[1,1,2]]

    # Normalising according to Fluxnet dataset statistics
    # float value : [minRg,maxRg, minTa, maxTa] = [0.0, 377.552, -13.35, 31.184]
    [minRg,maxRg, minTa, maxTa] = [0, 378, -14, 32]
    R = (Rg - minRg)/(maxRg - minRg)
    T = (Ta - minTa)/(maxTa - minTa)
   
    # Calculate correlation function looping over selected powers
    CorrFunc = []
    for p in powers:
        i,j,k = p
        C_ijk = np.ma.sum((N**i)*(R**j)*(T**k),axis=2)
        CorrFunc.append(C_ijk)

    return CorrFunc

def calculate_NEP_model(CorrFunc,NGD_year):
    """
    Convert time index relative to the Epoch, to a specific format 
    Calculate NEP model and uncertainty according to SOCCROP methodology
    Parameters
    ----------
    CorrFunc: list of float numpy.array(X_len,Y_len,T_len)
         Correlation functions between NDVI, Rg and Ta
    NGD_year: float numpy.array(X_len,Y_len,T_len)
         Number of green days
                   
    Returns
    -------
    nep: float numpy.array(X_len,Y_len,)
             Modelized Net Ecosystem Production in gC/m² (NEP)
    uncertainty: float numpy.array(X_len,Y_len,)
             Estimated uncertainty of the NEP in gC/m²

    """
    import numpy as np

    # Variables Indexing and Uncertainties Indexing [0,1,5,7] ?
    #var_idx = [0,1,2,3,4,5,6]
    #unc_idx = [0,1,2,3,4,5,6]

    # Construct variables and uncertainties primitives
    CorrFunc.append(NGD_year)
    Xvar = np.ma.stack(CorrFunc, axis=2)
    Xunc = np.ma.stack(CorrFunc, axis=2)

    # Hard coded model coefficient for NEP model with R² = 82.1%
    #nep_coefs = np.array([76.5, -60.8, -140.3, 67.5, -40.2, 150.6, -2.8])
    #uncertainty_coefs = np.array([0,0,0,0,0,0,0])
    nep_coefs = np.array([20.0, -2.8])
    uncertainty_coefs = np.array([0,0])

    # Construct model prediction
    nep = Xvar.dot(nep_coefs)

    # Construct uncertainty
    uncertainty = Xunc.dot(uncertainty_coefs)

    return nep, uncertainty

def get_carbon_tier1(self):
    """
    Compute Carbon Tier 1 - Brut force version that might do the job
    input: Interpolated NDVI time serie over on year + ERA5 radiation and temperature as exogeneous data
    output: 5 uint8 bands = Proxy of meteo data + NDAV over 4 trimesters
    """
    import numpy as np
    import numpy.ma as ma
    import datetime

    # Some Parameters
    no_data = -10000
    threshold = 0.3
    zeroK = -273.15

    print("DEBUG: CT1 calculation started")
    #print(dir(self))
    #print("self.interpolated_dates:",self.interpolated_dates)
    #print("self.raw_dates:",self.raw_dates)
    #    DEBUG
    #    print('all_dates.shape:',self.all_dates.shape)
    #    print('interpolated_dates.shape:',self.interpolated_dates.shape)
    #    print('raw_dates.shape:',self.raw_dates.shape)

    # Get ERA5 encoded data if they exist

    print("DEBUG: Gettng ERA5 data if available")
    try:
        exo_name = self.exogeneous_data_name
        print("DEBUG: exo_name:",exo_name)
    except:
        print("DEBUG: exo_name fail")

    try:
        era5encoded = self.get_exogeneous_data()
        print("DEBUG: era5encoded.shape", era5encoded.shape)
        print("DEBUG: era5encoded[0,0] = ", era5encoded[0,0])
        Rg = (era5encoded/10000).round().astype(int)
        Ta = ((era5encoded-10000*Rg) + zeroK).round().astype(int)
        print("DEBUG: Rg.shape", Rg.shape)
        print("DEBUG: Ta.shape", Ta.shape)
        print("DEBUG: Ta[-,-]", Ta[0,0])
        print("DEBUG: avg Rg = ", np.nanmean(Rg),"W/m2")
        print("DEBUG: avg Ta = ", np.nanmean(Ta),"°C")
        is_era5 = True
    except:
        print("DEBUG: temp.shape fail")
        is_era5 = False

    #try:
    #    era5array = self.exogeneous_data_array
    #    #era5encoded = np.moveaxis(era5array,0,-1)
    #except:
    #    print("DEBUG: era5array.shape fail")

    # Manipulate interpolated dates to get picked hemisphere
    idates_str = self.interpolated_dates['Sentinel2']
    idates = np.array([int(x) for x in idates_str])
    yyyy = int(idates[0]/10000)
    y = yyyy*10000
    first_month = (idates[0] % yyyy)/100

    if first_month < 6:
        print("DEBUG: Souther hemisphere period detected")
        #dates_periods = [idates[0],y+701,y+1001,y+10101,idates[-1]+1]
        dates_periods = [y+401,y+701,y+1001,y+10101,y+10401]
    else:
        print("DEBUG: Norther hemisphere period detected")
        dates_periods = [y+1001,y+10101,y+10401,y+10701,y+11001]
                               


    print("DEBUG: dates_periods",dates_periods)
    all_year_period = np.logical_and(np.greater_equal(idates,dates_periods[0]), np.less(idates,dates_periods[-1])) 
    print("DEBUG: all_year_period.shape", all_year_period.shape)
    period_masks = [np.logical_and(np.greater_equal(idates,m), np.less(idates,n)) for m,n in zip(dates_periods, dates_periods[1:])]
    period_sizes = [np.sum(x) for x in period_masks]
    print("DEBUG: period_size",period_sizes,"sum(period_size)",sum(period_sizes))

    # Evaluate number of day of active vegetation over entire year
    # Robust NDVI -> see acor_feat option
    ndvi = (self.get_interpolated_Sentinel2_NDVI()/1000.0)
    ndvi_masked = ma.masked_where(ndvi<=-1,ndvi)
    ndvi_year_period = ndvi_masked[:,:,all_year_period]
    print("DEBUG: ndvi_masked.shape",ndvi_masked.shape)
    print("DEBUG: ndvi_year_period.shape",ndvi_year_period.shape)
    #nb_days_veg   = np.ma.sum(np.ma.greater(ndvi_masked,threshold),axis=2).astype(np.int16) # -> Not save in this version
    nb_days_veg   = np.ma.sum(np.ma.greater(ndvi_year_period,threshold),axis=2).astype(np.int16)

    # Create bug gdal uint8 ???
    #nb_days_veg_periods = [np.ma.sum(np.ma.greater(ndvi_masked[:,:,x],threshold),axis=2).astype(np.uint8) for x in period_masks]
    nb_days_veg_periods = [np.ma.sum(np.ma.greater(ndvi_masked[:,:,x],threshold),axis=2).astype(np.uint16) for x in period_masks]

    # Obsolete Linear model for Annual Carbon flux estimation. Might be calculated as post processing
    #co2_flux = -0.0258*nb_days_veg + 1.685
    
    # Data type conversions for the global indicator
    indicator_ma = nb_days_veg
    global_indicator = indicator_ma.filled(no_data)

    # Data type conversions for the per period indicator
    indicator_ma_period = nb_days_veg_periods
    period_indicator = [x.filled(no_data) for x in indicator_ma_period]


    #print("DEBUG: global_indicator.shape = ",global_indicator.shape)
    print("DEBUG: CT1 Done")

    # Different output if ERA5 data available 
    if is_era5:
        print("DEBUG: ERA5 is used")

        # Calculate climatic correlation functions
        #CorrFunc = calculate_correlation_functions(ndvi_year_period, Rg, Ta)
        #CorrFunc = CorrFunc_masked.filled(no_data)

        # Calculate CT1 indicator with climatic correlation function methodology # NO VALIDATION YET
        #NEP_model_masked, NEP_model_uncertainty_masked = calculate_NEP_model(CorrFunc,global_indicator)

        
        # Calculate CT1 indicator with climatic correction  # NO VALIDATION YET
        Rg_threshold = 65.0
        NDVI_less = np.ma.masked_less(ndvi_year_period,threshold)
        Rg_greater   = np.ma.masked_greater(Rg, Rg_threshold)
        ngd = nb_days_veg
        G_01  = np.ma.sum(np.ma.logical_and(NDVI_less,Rg_greater),axis=2).astype(np.int16) # -> Not save in this version
        c1 = -0.85
        s1 = 0.24
        c2 = 0.14
        s2 = 0.50
        NEP_model_masked = c1*ngd + c2*G_01
        NEP_model_uncertainty_masked =  np.ma.sqrt(s1*s1*ngd*ngd + s2*s2*G_01*G_01)


        # Fill masks
        NEP_model             = NEP_model_masked.filled(no_data)
        NEP_model_uncertainty = NEP_model_uncertainty_masked.filled(no_data) 

        # Gather variables
        indicator = np.stack((period_indicator[0],
                              period_indicator[1],
                              period_indicator[2],
                              period_indicator[3],
                              NEP_model,
                              NEP_model_uncertainty), axis = 2)

        label = ["NGD_Trimester_1", "NGD_Trimester_2", "NGD_Trimester_3" "NGD_Trimester_4","NEP_Model", "NEP_Model_Uncertainty"]
    else:
        print("DEBUG: ERA5 data is not used")
        indicator = np.stack((global_indicator,
                              period_indicator[0],
                              period_indicator[1],
                              period_indicator[2],
                              period_indicator[3]), axis=2)
        label = ["NGD_year","NGD_Trimester_1", "NGD_Trimester_2", "NGD_Trimester_3" "NGD_Trimester_4""]

    return indicator, label
