##########################
# Get ERA5 data from API #
# Ludo 2021              #
##########################

#!/usr/bin/env python3
import numpy as np
import xarray as xr
import argparse
#from osgeo import ogr, osr
from osgeo import osr
from osgeo import ogr

#import osr
#import ogr

import os
import glob
import cdsapi # ERA5 API libray

#try:
#    from osgeo import ogr
#    from osgeo import osr
#    from osgeo import gdal
#except ImportError:
#    import gdal
#    import ogr
#    import osr

import rasterio
from rasterio import features
from rasterio.transform import Affine
from pyproj import Proj, transform
import infolog
c = cdsapi.Client()

# Some info:
# SAFYE-CO2 variables in dataset 'reanalysis-era5-single-levels'
# 2m temperature        K --> C.
# Mean surface downward short-wave radiation flux    W m-2 --> MJ.m-2.d-1
# Mean potential evaporation rate       kg m-2 s-1 --> mm.d-1
# Total precipitation   m/3h --> mm.d-1
#
# ['2m_temperature', 'potential_evaporation','surface_solar_radiation_downwards','total_precipitation'],

# To run
# - base env
# - command: python3 ./downloadERA5_v2.py -r tiles_ref/T30TYP.tif tiles_ref/T31TCJ.tif -t T30TYP T31TCJ -y 2019 -e North

# Some parameters/constants
###temperature_statistics = 'day_time_mean'
# - Obsolete -
#ERA5database = "sis-agrometeorological-indicators"
#temperature_statistics = '24_hour_mean'
#ERA5dataset = None
ERA5database = "reanalysis-era5-land"
ERA5dataset  = "reanalysis-era5-single-levels"
Rg_var = "surface_solar_radiation_downwards"
Ta_var = "2m_temperature"
delta = (1/8)



def get_area_coordinates(im):
    area_list = []
    with rasterio.open(im) as src:
        ras_profile = src.profile
        ras_meta = src.meta
        l,b,r,t = src.bounds
        bbox = [[l,b],[r,t]]
        in_crs = src.crs.to_epsg()
 
        out_crs = 4326
        InSR = osr.SpatialReference()
        InSR.ImportFromEPSG(in_crs)
        OutSR = osr.SpatialReference()
        OutSR.ImportFromEPSG(out_crs)
        Point = ogr.Geometry(ogr.wkbPoint)
        area = [0,0,0,0]

        Point.AddPoint(float(bbox[0][0]),float(bbox[0][1])) # use your coordinates here
        Point.AssignSpatialReference(InSR)    # tell the point what coordinates it's in
        Point.TransformTo(OutSR)              # project it to the out spatial reference
        area[1] = Point.GetX()
        area[0] = Point.GetY()

        Point.AddPoint(float(bbox[1][0]),float(bbox[1][1])) # use your coordinates here
        Point.AssignSpatialReference(InSR)    # tell the point what coordinates it's in
        Point.TransformTo(OutSR)              # project it to the out spatial reference
        area[3] = Point.GetX()
        area[2] = Point.GetY()
        area_list.append(area)
        #print("Area for " + im)
        #print("    ", area)
        #print

    area_list = np.array(area_list)
    area = [0,0,0,0]
    area[0] = np.min(area_list[:,0])
    area[1] = np.min(area_list[:,1])
    area[2] = np.max(area_list[:,2])
    area[3] = np.max(area_list[:,3])
    
    return area

if __name__ == "__main__":
    # Initialize Logger 
    log = infolog.infolog()

    # Initialise Parser
    parser = argparse.ArgumentParser()
    #optional = parser._action_groups.pop() # Edited this line
    required = parser.add_argument_group('required arguments')
    required.add_argument("-r", "--referenceimage", nargs = "+", help="Reference Raster Image (str)")
    required.add_argument("-t", "--tile", nargs = "+", required=True, help="Reference Raster Image tile (str)")
    required.add_argument("-y", "--year", type  = int,required=True, help="Agricultural year (int)")
    required.add_argument("-e", "--hemisphere", required=True, choices=['North',"South"], help="Considere hemisphere (str)")
    #required.add_argument("-a", "--area", nargs = "+", help="Lat-Lon coordinate of bounding box area (minlat,minlong,maxlat,maxlong)")
    #required.add_argument("-fd", "--coordinates", nargs = "+", required=True, help="List of coordinates")
    #parser._action_groups.append(optional) # added this line
    args=parser.parse_args()

    # Input parameter variables
    reference_names    = args.referenceimage
    tiles              = args.tile
    year              = args.year
    hemisphere        = args.hemisphere

    # Create work and output director
    try:
        os.mkdir("workdir")
    except:
        pass

    try:
        os.mkdir("outputs")
    except:
        pass

    # get area from ref
    for reference_name,tile in zip(reference_names,tiles):
        log.msg("Gettig ERA5 variable over %s tile"%(tile))

        # Create work directory if does not exist
        tiledir="workdir/%s_%s"%(tile,year)
        try:
            #print(tiledir)
            os.mkdir(tiledir)
        except:
            pass

        # Get request area 
        area = get_area_coordinates(reference_name)

        # Add some space to insure the overlap with S2 tiles
        area[0] = area[0] - delta
        area[1] = area[1] - delta
        area[2] = area[2] + delta
        area[3] = area[3] + delta
        log.msg("Area:")
        print(area)

        # Initialise tile/year variables
        VarList  = [Rg_var, Ta_var]
        yy1 = "%d"%(year - 1)
        yy2 = "%d"%(year)
        dd = ["%02d"%(i) for i in range(1,31+1)]
        tt = ['00:00', '01:00', '02:00',
              '03:00', '04:00', '05:00',
              '06:00', '07:00', '08:00',
              '09:00', '10:00', '11:00',
              '12:00', '13:00', '14:00',
              '15:00', '16:00', '17:00',
              '18:00', '19:00', '20:00',
              '21:00', '22:00', '23:00']

        months = {"North":  {yy1:[['10','11','12']], yy2:[['01','02','03'],['04','05','06'],['07','08','09']]},
                  "South":  {yy1:[['04','05','06'],['07','08','09'],['10','11','12']], yy2:[['01','02','03']]}}
        
        request_template = {'dataset': ERA5dataset,
                            'product_type': 'reanalysis',
                            'format': 'netcdf',
                            'area':area,
                            'day':dd,
                            'time':tt}

        # Variable loop
        for var in VarList:
            log.msg("Variable: %s"%(var))
            for yy in [yy1,yy2]:
                log.msg("  Year: %s"%(yy))
                for i,mm in enumerate(months[hemisphere][yy]):
                    #log.msg("    Months: ", mm)

                    # Construct request per month
                    request = request_template
                    request = dict(request, **{'variable':var, 'year': yy, 'month': mm})

                    out_name = "%s/%s_%s_%s_block%d.nc"%(tiledir,tile,var,yy,i)

                    #print(out_name,request)

                    is_request = True
                    if is_request:
                         c.retrieve(ERA5dataset, request, out_name)
        

        log.msg("Merging all data set for tile %s"%(tile))
        radiation_nc   = sorted(glob.glob('%s/*surface_solar_radiation_downwards*.nc'%(tiledir)))
        temperature_nc = sorted(glob.glob('%s/*2m_temperature*.nc'%(tiledir)))

        #print(radiation_nc)
        #print(temperature_nc)

        encoded_data = []
        #for rg_file, ta_file in zip(radiation_nc[:2],temperature_nc[:2]):
        for rg_file, ta_file in zip(radiation_nc,temperature_nc):
            # Radiation in J/m2 daily cumulated to W/m2
            rg = (xr.open_dataset(rg_file).resample(time="D").sum()/86400).round().astype(int)
            # Temperature in K averaged daily
            ta = xr.open_dataset(ta_file).resample(time="D").mean().round().astype(int)
            # Encoding data
            rg["encoded"] = 10000*rg["ssrd"] + ta["t2m"]
            encoded_data.append(rg["encoded"])

        ds = xr.merge(encoded_data).astype(np.uint32)
        ds.to_netcdf(path="%s/%s_%s_ERA5_encoded_data.nc"%("outputs",tile,year))

        log.msg("Done")
